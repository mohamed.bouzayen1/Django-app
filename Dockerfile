FROM python:3.8
WORKDIR /App
COPY  . /App/
ENV PATH="${PATH}:/root/.local/bin"
RUN pip install -r ./requirements.txt --user
RUN PYTHONPATH=/App/
ENTRYPOINT ["python","./manage.py","runserver","0.0.0.0:8000"]